<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>


<c:set var="contextPath" value="${pageContext.request.contextPath}" />

<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<title>Drools Web Application</title>

<!-- Icons font CSS-->
<link
	href="${contextPath }/resources/vendor/mdi-font/css/material-design-iconic-font.min.css"
	rel="stylesheet" media="all">
<link
	href="${contextPath }/resources/vendor/font-awesome-4.7/css/font-awesome.min.css"
	rel="stylesheet" media="all">
<!-- Font special for pages-->
<link
	href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
	rel="stylesheet">

<!-- Vendor CSS-->
<link href="${contextPath }/resources/vendor/select2/select2.min.css"
	rel="stylesheet" media="all">

<!-- Material Design for Bootstrap CSS -->
<link rel="stylesheet"
	href="${contextPath }/resources/css/bootstrap-material-design.css" />
<link rel="stylesheet"
	href="${contextPath }/resources/css/jquery-ui.css" />
<!-- <link rel="stylesheet" href="${contextPath }/resources/css/bootstrap.min.css" />-->
<link rel="stylesheet"
	href="${contextPath }/resources/css/typography.css" />
<!-- Main CSS-->
<link href="${contextPath }/resources/css/main.css" rel="stylesheet"
	media="all">
</head>

<body>
	<h3 style="text-align: right;">Rakesh(Treasury Officer)</h3>
	<div class="page-wrapper bg-gra-02 p-t-130 p-b-100 font-poppins">
		<div class="wrapper wrapper--w780">
			<div class="card card-4">
				<div class="card-body">
					<h2 class="title">List Of Tasks</h2>
					<table class="table table-striped table-dark"
						id="managerTaskMaintenance">
						<thead>
							<tr>
								<th scope="col" style="color: white;">Employee Code</th>
								<th scope="col" style="color: white;">Employee Name</th>
								<th scope="col" style="color: white;">Salary</th>
								<th scope="col" style="color: white;">Commission</th>
								<th scope="col" style="color: white;">Hike Percentage</th>
								<th scope="col" style="color: white;">Basic Pay</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${employeeList }" var="employee">
								<tr>
									<td>${employee.empId }</td>
									<td>${employee.empName }</td>
									<td>${employee.empSalary }</td>
									<td>${employee.empCommission }th Pay</td>
									<td>
										<div id="HB${employee.empId }">
											<button type="button" class="btn btn-primary btn-sm"
												style="background-color: blue; color: white;"
												onClick="getHike('${employee.empId }','${employee.empName }','${employee.empSalary }','${employee.empCommission }')">Get
												Hike</button>
										</div>
										<div id="HT${employee.empId }"></div>
									</td>
									<td>
										<div id="LB${employee.empId }">
											<button type="button" class="btn btn-primary btn-sm"
												style="background-color: blue; color: white;"
												onClick="getBasicPay('${employee.empId }','${employee.empName }','${employee.empSalary }','${employee.empCommission }')">Get
												Base Pay</button>
										</div>
										<div id="LT${employee.empId }"></div>
									</td>
								</tr>
							</c:forEach>
						</tbody>

					</table>
				</div>
			</div>
		</div>
	</div>
	<!-- Jquery JS-->
	<script src="${contextPath }/resources/vendor/jquery/jquery.min.js"></script>
	<!-- Vendor JS-->
	<script src="${contextPath }/resources/vendor/select2/select2.min.js"></script>
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script type="text/javascript"
		src="${contextPath }/resources/js/jquery-3.3.1.min.js"></script>
	<!-- Bootstrap tooltips -->
	<script type="text/javascript"
		src="${contextPath }/resources/js/popper.min.js"></script>
	<!-- Bootstrap core JavaScript -->
	<script type="text/javascript"
		src="${contextPath }/resources/js/bootstrap.min.js"></script>
	<script src="${contextPath }/resources/js/bootstrap-material-design.js"></script>
	<script type="text/javascript"
		src="${contextPath }/resources/js/jquery-ui.js"></script>

	<script>
		function getHike(empId, empName, empSalary, empCommission) {
			var jsonResponse = {};
			jsonResponse.empId = empId;
			jsonResponse.empName = empName;
			jsonResponse.empSalary = empSalary;
			jsonResponse.empCommission = empCommission;
			$
					.ajax({
						type : "POST",
						url : "gethike",
						data : JSON.stringify(jsonResponse),
						contentType : "application/json; charset=utf-8",
						success : function(result, success, jqXHR) {
							var ct = jqXHR.getResponseHeader('content-type')
									|| "";
							if (ct.indexOf('text/html') >= 0) {
								var errorString = 'Something went wrong. Please refresh and try again';
								alert(errorString);
							} else {
								document.getElementById("HB" + empId).style.display = 'none';
								document.getElementById("HT" + empId).innerHTML = "Hike is - "
										+ result + "%";
							}
						},
						error : function() {
							var errorString = 'Something went wrong. Please refresh and try again';
							alert(errorString);
						}
					});
		}
		function getBasicPay(empId, empName, empSalary, empCommission) {
			var jsonResponse = {};
			jsonResponse.empId = empId;
			jsonResponse.empName = empName;
			jsonResponse.empSalary = empSalary;
			jsonResponse.empCommission = empCommission;
			$
					.ajax({
						type : "POST",
						url : "getBasicPay",
						data : JSON.stringify(jsonResponse),
						contentType : "application/json; charset=utf-8",
						success : function(result, success, jqXHR) {
							var ct = jqXHR.getResponseHeader('content-type')
									|| "";
							if (ct.indexOf('text/html') >= 0) {
								var errorString = 'Something went wrong. Please refresh and try again';
								alert(errorString);
							} else {
								document.getElementById("LB" + empId).style.display = 'none';
								document.getElementById("LT" + empId).innerHTML = "Base Pay - "
										+ result;
							}
						},
						error : function() {
							var errorString = 'Something went wrong. Please refresh and try again';
							alert(errorString);
						}
					});
		}
	</script>
</body>

</html>