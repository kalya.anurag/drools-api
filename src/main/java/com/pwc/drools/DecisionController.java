package com.pwc.drools;

import java.util.ArrayList;
import java.util.List;

import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.pwc.drools.model.Salary;

@Controller
public class DecisionController {

	/*
	 * private final KieContainer kieContainer;
	 * 
	 * public DecisionController(KieContainer kieContainer) { this.kieContainer =
	 * kieContainer; }
	 */
	
	@Autowired
    private KieContainer kieContainer;

	@GetMapping("/employees")
	private String getEmployees(ModelMap model) {
		List<Salary> list = new ArrayList<Salary>();
		list.add(new Salary("E001", "Anurag Kalya", 80000, 7));
		list.add(new Salary("E002", "Vipul Malpani", 100000, 6));
		list.add(new Salary("E003", "Sushil", 120000, 7));
		list.add(new Salary("E004", "Rajeesh Sharma", 120000, 5));
		model.put("employeeList", list);
		return "droolHome";
	}

	@PostMapping("/gethike")
	private @ResponseBody String getHikePercent(@RequestBody Salary salaryReq) {
		KieSession kieSession = kieContainer.newKieSession();
		kieSession.insert(salaryReq);
		kieSession.fireAllRules();
		kieSession.dispose();
		System.out.println("HIKE: " + salaryReq.getEmpHike());
		return String.valueOf(salaryReq.getEmpHike());
	}
	
	@PostMapping("/getLeaves")
	private @ResponseBody String getLeaves(@RequestBody Salary salaryReq) {
		KieSession kieSession = kieContainer.newKieSession();
		kieSession.insert(salaryReq);
		kieSession.fireAllRules();
		kieSession.dispose();
		System.out.println("Leaves: " + salaryReq.getEmpLeaves());
		return String.valueOf(salaryReq.getEmpLeaves());
	}
	
	@PostMapping("/getBasicPay")
	private @ResponseBody String getBasicPay(@RequestBody Salary salaryReq) {
		KieSession kieSession = kieContainer.newKieSession();
		kieSession.insert(salaryReq);
		kieSession.fireAllRules();
		kieSession.dispose();
		
		//salaryReq.setEmpBasePay(Math.min(salaryReq.getEmpSubstanceGrant(), salaryReq.getEmpSalary() * 0.5));
		return String.valueOf(salaryReq.getEmpBasePay());
	}
		
}
