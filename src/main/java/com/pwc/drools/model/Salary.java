package com.pwc.drools.model;

public class Salary {
	private String empId;
	private String empName;
	private int empSalary;
	private int empHike;
	private String empType;
	private int empCommission;
	private int empLeaves;
	private int empConfPct;
	private int empSubstanceGrant;
	private double empBasePay;

	public Salary(String empId, String empName, int empSalary, int empCommission) {
		super();
		this.empId = empId;
		this.empName = empName;
		this.empSalary = empSalary;
		this.empCommission = empCommission;
	}

	/**
	 * @return the empId
	 */
	public String getEmpId() {
		return empId;
	}

	/**
	 * @param empId the empId to set
	 */
	public void setEmpId(String empId) {
		this.empId = empId;
	}

	/**
	 * @return the empName
	 */
	public String getEmpName() {
		return empName;
	}

	/**
	 * @param empName the empName to set
	 */
	public void setEmpName(String empName) {
		this.empName = empName;
	}

	/**
	 * @return the empSalary
	 */
	public int getEmpSalary() {
		return empSalary;
	}

	/**
	 * @param empSalary the empSalary to set
	 */
	public void setEmpSalary(int empSalary) {
		this.empSalary = empSalary;
	}

	/**
	 * @return the empHike
	 */
	public int getEmpHike() {
		return empHike;
	}

	/**
	 * @param empHike the empHike to set
	 */
	public void setEmpHike(int empHike) {
		this.empHike = empHike;
	}

	/**
	 * @return the empLeaves
	 */
	public int getEmpLeaves() {
		return empLeaves;
	}

	/**
	 * @param empLeaves the empLeaves to set
	 */
	public void setEmpLeaves(int empLeaves) {
		this.empLeaves = empLeaves;
	}

	/**
	 * @return the empType
	 */
	public String getEmpType() {
		return empType;
	}

	/**
	 * @param empType the empType to set
	 */
	public void setEmpType(String empType) {
		this.empType = empType;
	}

	/**
	 * @return the empConfPct
	 */
	public int getEmpConfPct() {
		return empConfPct;
	}

	/**
	 * @param empConfPct the empConfPct to set
	 */
	public void setEmpConfPct(int empConfPct) {
		this.empConfPct = empConfPct;
	}

	/**
	 * @return the empSubstanceGrant
	 */
	public int getEmpSubstanceGrant() {
		return empSubstanceGrant;
	}

	/**
	 * @param empSubstanceGrant the empSubstanceGrant to set
	 */
	public void setEmpSubstanceGrant(int empSubstanceGrant) {
		this.empSubstanceGrant = empSubstanceGrant;
	}

	/**
	 * @return the empCommission
	 */
	public int getEmpCommission() {
		return empCommission;
	}

	/**
	 * @param empCommission the empCommission to set
	 */
	public void setEmpCommission(int empCommission) {
		this.empCommission = empCommission;
	}

	/**
	 * @return the empBasePay
	 */
	public double getEmpBasePay() {
		return empBasePay;
	}

	/**
	 * @param empBasePay the empBasePay to set
	 */
	public void setEmpBasePay(double empBasePay) {
		this.empBasePay = empBasePay;
	}

}
